from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.

class story9Test(TestCase):

    def test_url_is_exsist(self):
        response = Client().get('/Story9/')
        self.assertEqual(response.status_code,200)

    def test_Story9_using_page_template(self):
        response = Client().get('/Story9/')
        self.assertTemplateUsed(response, 'Story9/page.html')

    def test_Story8_using_homepage_func(self):
        found = resolve('/Story9/')
        self.assertEqual(found.func,page)
