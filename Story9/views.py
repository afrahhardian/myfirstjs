from django.shortcuts import render
from django.http import JsonResponse, HttpResponse

# Create your views here.
response = {}
def page(request):
    if request.user.is_authenticated:
        request.session['username'] = request.user.username
        response['username'] = request.session['username']
        if 'counter' not in request.session:
            request.session['counter'] = 0
        response['counter'] = request.session['counter']
        print(dict(request.session))
    return render(request,'Story9/page.html',response)

def addFav(request):
    print(dict(request.session))
    request.session['counter'] = request.session['counter'] + 1
    return HttpResponse(request.session['counter'], content_type = 'application/json')

def deleteFav(request):
    request.session['counter'] = request.session['counter'] - 1
    return HttpResponse(request.session['counter'], content_type = 'application/json')
