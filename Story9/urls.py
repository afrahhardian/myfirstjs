from django.urls import path
from .views import page, addFav, deleteFav
from django.contrib.auth import views as auth_views


app_name = "Story9"
urlpatterns = [
    path('', page, name='page'),
    path('addFav', addFav, name = 'addFav'),
    path('deleteFav', deleteFav, name = 'deleteFav'),

]
