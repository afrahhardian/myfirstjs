$( document ).ready(function() {
    console.log( "ready!" );
    $("#mainContainer").hide();
    setInterval(hideSpinner, 5000);
    setInterval(showMainContainer,5000)
});

function hideSpinner(){
  $(".loader").hide();
}

function showMainContainer(){
  $("#mainContainer").show();
}



$(".accordion").on("click", ".accordion-header", function() {
 $(this).toggleClass("active").next().slideToggle();
});

var isChanged = false;
function changeTheme(){
  if(isChanged){
    document.body.style.background = "linear-gradient(to bottom right, #3a1c71, #d76d77, #ffaf7b)";
    document.getElementById("title").style.color = "black";
    isChanged = false;
  }
  else{
    document.body.style.background = "linear-gradient(90deg, #FDBB2D 0%, #22C1C3 100%)";
    document.getElementById("title").style.color = "white";
    isChanged = true;
  }

}
