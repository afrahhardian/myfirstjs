$(document).ready(function() {
  $("#retrivelist").click(function() {
    var displaylist = $("#display-resources");

    displaylist.text("Loading data from JSON source...");

    $.ajax({
      type: "GET",
      url: 'https://www.googleapis.com/books/v1/volumes?q=quilting',
      dataType: 'json',

      success: function(result){
        console.log(result);
        var output =  "<table class='table'><thead class = 'thead-dark'><tr><th scope='col'>Title of Book</th><th scope='col'>Author</th><th scope='col'>Add to Favorite</th></thead><tbody>";
        for(var i = 0; i < result.items.length; i++){
          output +=
          "<tr><td>" +
          result.items[i].volumeInfo.title +
          "</td><td>" +
          result.items[i].volumeInfo.authors +
          "</td><td>" +
          "<button type='button' id = 'add' class='btn btn-primary'>Add Fav</button>" +
          "</td></tr>";
        }
        output += "</tbody></table>";
        displaylist.html(output);
        $("table").addClass("table");
      }


    });

    });

    $(document).on('click', '#add', function (){
      var counterfavString = $("#counter").text();
      var counterfavInt = parseInt(counterfavString);
      console.log($(this).text());
      if($(this).text() === 'Add Fav'){
        // var countResult = counterfavInt + 1;
        // $("#counter").text(countResult);
        $(this).text("Favourited");
        $.ajax({
          type: "GET",
          url: '/Story9/addFav',
          success: function(data){
            console.log(data)
            $("#counter").empty().text(data);
          }
        })
      }
      else{
        var countResult = counterfavInt - 1;
        $("#counter").text(countResult);
        $(this).text("Add Fav");
        $.ajax({
          type: "GET",
          url: '/Story9/deleteFav',
          success: function(data){
            $("#counter").empty().text(data);
          }
        })
      //   $.ajax({
      //     type: "GET",
      //     url: '/Story9/deleteFav',
      //     success: fuction(data){
      //         $("#counter").empty().text(data);
      //     }
      //
      //   })
      }

    });
});
