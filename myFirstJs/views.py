from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import logout as auth_logout

def login(request):
    return render(request, 'registration/login.html')

def logout(request):
    request.session.flush()
    auth_logout(request)
    return HttpResponseRedirect('/login')
