from django.test import TestCase, Client
from django.urls import resolve
from .views import *

# Create your tests here.
class story8Test(TestCase):

    def test_hompage_url_is_exsist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_Story8_using_home_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'Story8/home.html')

    def test_Story8_using_homepage_func(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)
